My first software project!

# Formulatr

### A Simple Android App For Simple Math (No longer available in the Google Play Store)

```
    Formulatr allows you to find values for area, volume, or other equations quicker and easier than most other apps. 

    Enter in the known variables in order to find the remaining value - no matter which variable it is.

    If you have a formula to add, feature, or bug you've found: email Steve.Olson05@gmail.com

    Uses portions of https://github.com/MarkyVasconcelos/Towel
```
