package com.code;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Steve on 7/5/13.
 */
public class equationLists {


    ArrayList<String> formulas = new ArrayList<String>();
    private static HashMap<String, myFormula> equations = new HashMap<String, myFormula>();
    private static HashMap<String, myFormula> areaEquations = new HashMap<String, myFormula>();
    private static HashMap<String, myFormula> surfaceAreaEquations = new HashMap<String, myFormula>();
    private static HashMap<String, myFormula> volumeEquations = new HashMap<String, myFormula>();
    private static HashMap<String, myFormula> otherEquations = new HashMap<String, myFormula>();
    private static HashMap<String, myFormula> favorites = new HashMap<String, myFormula>();

    public equationLists()
    {
        //AREA
        addAreaCircle();
        addAreaRectangle();
        addAreaSquare();
        addAreaParallelogram();
        addAreaTrapezoid();
        addAreaEllipse();
        addAreaTriangle();

        //VOLUME
        addVolumeSphere();
        addVolumeCube();
        addVolumeCylinder();
        addVolumePyramid();
        addVolumeRectangularPrism();
        addVolumeCone();

        //SURFACE AREA
        addSurfaceAreaCube();
        addSurfaceAreaSphere();
        addSurfaceAreaCylinder();
        addSurfaceAreaPrism();

        //OTHER
        addCubeRoot();
        addPythagoreanTheorem();
        addLinearEquation();
        addPowerCalculator();

        //Throws all the equations we have so far into the mass list
        equations.putAll(areaEquations);
        equations.putAll(volumeEquations);
        equations.putAll(surfaceAreaEquations);
        equations.putAll(otherEquations);
    }

    //AREA
    public void addAreaCircle() {
        resetFormulas();
        formulas.add(0, "3.14159*r*r");
        formulas.add(1, "sqrt(A/(3.14159))");
        myFormula areaCircle = new myFormula("Area Circle", 2, formulas, "Ar");
        areaEquations.put("Area Circle", areaCircle);
    }
    public void addAreaRectangle()  {
        resetFormulas();
        formulas.add(0, "w*h");
        formulas.add(1, "A/h");
        formulas.add(2, "A/w");
        myFormula areaRectangle = new myFormula("Area Rectangle", 3, formulas, "Awh");
        areaEquations.put("Area Rectangle", areaRectangle);
    }
    public void addAreaTriangle()   {
        resetFormulas();
        formulas.add(0, "b*h/2");
        formulas.add(1, "2*A/h");
        formulas.add(2, "2*A/b");
        myFormula areaTriangle = new myFormula("Area Triangle", 3, formulas, "Abh");
        areaEquations.put("Area Triangle", areaTriangle);
    }
    public void addAreaSquare() {
        resetFormulas();
        formulas.add(0, "w*w");
        formulas.add(1, "A^(1/2)");
        myFormula areaSquare = new myFormula("Area Square", 2, formulas, "Aw");
        areaEquations.put("Area Square", areaSquare);
    }
    public void addAreaParallelogram() {
        resetFormulas();
        formulas.add(0, "w*h");
        formulas.add(1, "A/h");
        formulas.add(2, "A/w");
        myFormula areaParallelogram = new myFormula("Area Parallelogram", 3, formulas, "Awh");
        areaEquations.put("Area Parallelogram", areaParallelogram);
    }
    public void addAreaTrapezoid()  {
        resetFormulas();
        formulas.add(0, "(h/2)*(b+c)");
        formulas.add(1, "2*A/(b+c)");
        formulas.add(2, "2*A/h-c");
        formulas.add(3, "2*A/h-b");
        myFormula areaTrapezoid = new myFormula("Area Trapezoid", 4, formulas, "Ahbc");
        areaEquations.put("Area Trapezoid", areaTrapezoid);
    }
    public void addAreaEllipse()    {
        resetFormulas();
        formulas.add(0, "3.14159*x*y");
        formulas.add(1, "A/(3.14159*y)");
        formulas.add(2, "A/(3.14159*x)");
        myFormula areaEllipse = new myFormula("Area Ellipse", 3, formulas, "Axy");
        areaEquations.put("Area Ellipse", areaEllipse);
    }

    //VOLUME
    public void addVolumeSphere()   {
        resetFormulas();
        formulas.add(0, "(r^3)*3.14159*4/3");
        formulas.add(1, "(3*V/(4*3.14159))^(1/3)");
        myFormula volumeSphere = new myFormula("Volume Sphere", 2, formulas, "Vr");
        volumeEquations.put("Volume Sphere", volumeSphere);
    }
    public void addVolumeCube() {
        resetFormulas();
        formulas.add(0, "w^3");
        formulas.add(1, "V^(1/3)");
        myFormula volumeCube = new myFormula("Volume Cube", 2, formulas, "Vw");
        volumeEquations.put("Volume Cube", volumeCube);
    }
    public void addVolumeRectangularPrism() {
        resetFormulas();
        formulas.add(0, "l*w*h");
        formulas.add(1, "V/(l*w)");
        formulas.add(2, "V/(w*h)");
        formulas.add(3, "V/(l*h)");
        myFormula volumeRectangularPrism = new myFormula("Volume Rectangular Prism", 4, formulas, "Vhlw");
        volumeEquations.put("Volume Rectangular Prism", volumeRectangularPrism);
    }
    public void addVolumeCylinder() {
        resetFormulas();
        formulas.add(0, "(r^2)*h*3.14159");
        formulas.add(1, "(V/(3.14159*h))^(1/2)");
        formulas.add(2, "V/(3.14159*(r^2))");
        myFormula volumeCylinder = new myFormula("Volume Cylinder", 3, formulas, "Vrh");
        volumeEquations.put("Volume Cylinder", volumeCylinder);
    }
    public void addVolumePyramid()  {
        resetFormulas();
        formulas.add(0, "w*h/3");
        formulas.add(1, "V*3/h");
        formulas.add(2, "V*3/w");
        myFormula volumePyramid = new myFormula("Volume Pyramid", 3, formulas, "Vwh");
        volumeEquations.put("Volume Pyramid", volumePyramid);
    }
    public void addVolumeCone() {
        resetFormulas();
        formulas.add(0, "(r^2)*3.14159*h/3");
        formulas.add(1, "3*V/(3.14159*h)^(0.5)");
        formulas.add(2, "3*V/(3.14159*(r^2))");
        myFormula volumeCone = new myFormula("Volume Cone", 3, formulas, "Vrh");
        volumeEquations.put("Volume Cone", volumeCone);
    }

    //SURFACE AREA
    public void addSurfaceAreaCube()  {
        resetFormulas();
        formulas.add(0, "6*(w^2)");
        formulas.add(1, "(S/6)^0.5");
        myFormula surfaceAreaCube = new myFormula("Surface Area Cube", 2, formulas, "Sw");
        surfaceAreaEquations.put("Surface Area Cube", surfaceAreaCube);
    }
    public void addSurfaceAreaSphere()  {
        resetFormulas();
        formulas.add(0, "4*3.14159*(r^2)");
        formulas.add(1, "(S/(4*3.14159))^0.5");
        myFormula surfaceAreaSphere = new myFormula("Surface Area Sphere", 2, formulas, "Sr");
        surfaceAreaEquations.put("Surface Area Sphere", surfaceAreaSphere);
    }
    public void addSurfaceAreaPrism()  {
        resetFormulas();
        formulas.add(0, "2*w*l+2*l*h+2*w*h");
        formulas.add(1, "(S/2-l*h)/(l+h)");
        formulas.add(2,"(S/2-w*h)/(w+h)");
        formulas.add(3, "(S/2-w*l)/(w+l)");
        myFormula surfaceAreaPrism = new myFormula("Surface Area Prism", 4, formulas, "Swlh");
        surfaceAreaEquations.put("Surface Area Prism", surfaceAreaPrism);
    }
    public void addSurfaceAreaCylinder()  {
        resetFormulas();
        formulas.add(0, "2*3.14159*r*(h+r)");
        formulas.add(1, "((0-h)+sqrt(h^2+2*S/3.14159))/2");
        formulas.add(2, "(S-2*3.14159*(r^2))/(2*3.14159*r)");
        myFormula surfaceAreaCylinder = new myFormula("Surface Area Cylinder", 3, formulas, "Srh");
        surfaceAreaEquations.put("Surface Area Cylinder", surfaceAreaCylinder);
    }

    //OTHER
    public void addCubeRoot()  {
        resetFormulas();
        formulas.add(0, "B^(1/3)");
        formulas.add(1, "A*A*A");
        myFormula cubeRoot = new myFormula("Cube Root", 2, formulas, "AB");
        otherEquations.put("Cube Root", cubeRoot);
    }
    public void addPythagoreanTheorem() {
        resetFormulas();
        formulas.add(0,"sqrt(c^2-b^2)");
        formulas.add(1,"sqrt(c^2-a^2)");
        formulas.add(2,"sqrt(a^2+b^2)");
        myFormula pythagorean = new myFormula("Pythagorean Theorem", 3, formulas, "abc");
        otherEquations.put("Pythagorean Theorem", pythagorean);
    }
    public void addLinearEquation() {
        resetFormulas();
        formulas.add(0, "m*x+b");
        formulas.add(1, "(y-b)/x");
        formulas.add(2, "(y-b)/m");
        formulas.add(3, "y-m*x");
        myFormula linear = new myFormula("Linear Equation", 4, formulas, "ymxb");
        otherEquations.put("Linear Equation", linear);
    }
    public void addPowerCalculator() {
        resetFormulas();
        formulas.add(0, "y^(1/n)");
        formulas.add(1, "log(y)/log(x)");
        formulas.add(2, "x^n");
        myFormula powerCalculator = new myFormula("Power Calculator", 3, formulas, "xny");
        otherEquations.put("Power Calculator", powerCalculator);
    }


    public HashMap<String, myFormula> getList() {
        return equations;
    }

    public HashMap<String, myFormula> getAreaList() {
        return areaEquations;
    }

    public HashMap<String, myFormula> getSurfaceAreaList() {
        return surfaceAreaEquations;
    }

    public HashMap<String, myFormula> getVolumeList()   {
        return volumeEquations;
    }

    public HashMap<String, myFormula> getOtherList()    {
        return otherEquations;
    }

    public HashMap<String, myFormula> getFavorites()    {
        for (myFormula f : favorites.values())
        {
            Log.v("Favorite", f.getName());
        }
        return favorites;
    }

    public static myFormula getEquation(String name)    {
        return equations.get(name);
    }

    public void resetFormulas() {
        formulas = new ArrayList<String>();
    }

    public static void addFavorite(String name) {
        favorites.put(equations.get(name).getName(), equations.get(name));
    }

    public static void removeFavorite(String name)  {
        if (favorites.containsKey(name))  {
            favorites.remove(name);
        }
    }

    public static boolean isFavorite(String name)  {
        return favorites.containsKey(name);
    }

}
