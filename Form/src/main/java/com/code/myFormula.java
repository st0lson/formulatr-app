package com.code;

import java.util.ArrayList;

/**
 * Created by Steve on 7/5/13.
 */
public class myFormula {

    private String name;
    private int numVar;
    private ArrayList<String> formulas;
    private ArrayList<String> variables;

    public myFormula(String name, int numVar, ArrayList<String> formulas, String inputs) {
        this.name = name;
        this.numVar = numVar;
        this.formulas = formulas;
        this.variables = new ArrayList<String>();

        for(int i=0; i<numVar; i++) {
            variables.add(inputs.substring(i,i+1));
        }
    }

    public String getName() {
        return name;
    }

    public int getNumVars() {
        return numVar;
    }

    public String getFormula(int i){
        return formulas.get(i);
    }

    public ArrayList<String> getVariables() {
        return variables;
    }

    public String getVariable(int num) {
        return variables.get(num);
    }

}
