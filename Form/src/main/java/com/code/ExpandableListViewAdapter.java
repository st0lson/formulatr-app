package com.code;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.first.form.R;

import java.util.List;
import java.util.Map;


/**
 * Created by Steve on 8/6/13.
 */
public class ExpandableListViewAdapter extends BaseExpandableListAdapter {

    private Activity context;
    private Map<String, List<String>> collectionList;
    private List<String> formulas;

    public ExpandableListViewAdapter(Activity context, List<String> formulas, Map<String, List<String>> collectionList) {
        this.context = context;
        this.collectionList = collectionList;
        this.formulas = formulas;
    }

    public Object getChild(int groupPosition, int childPosition)    {
        return collectionList.get(formulas.get(groupPosition)).get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition)    {
        return childPosition;
    }

    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent)   {

        final String formula = (String) getChild(groupPosition, childPosition);
        LayoutInflater inflater = context.getLayoutInflater();

        if (convertView == null)   {
            convertView = inflater.inflate(R.layout.child_row, null);
        }

        TextView item = (TextView) convertView.findViewById(R.id.child_item);

        ImageView favButton = (ImageView) convertView.findViewById(R.id.favButton);

        favButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view)  {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Add to favorites?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes!",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                equationLists.addFavorite((String) getChild(groupPosition, childPosition));
                                notifyDataSetChanged();
                            }
                        });
                builder.setNegativeButton("No.",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialogInterface, int i) {
                                equationLists.removeFavorite((String) getChild(groupPosition, childPosition));
                                notifyDataSetChanged();
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });

        if (equationLists.isFavorite(formula))    {
            favButton.setImageResource(convertView.getResources().getIdentifier("formulate_fav_full", "drawable", "com.first.form"));
        }
        else    {
            favButton.setImageResource(convertView.getResources().getIdentifier("formulate_fav", "drawable", "com.first.form"));
        }

        item.setText(formula);
        return convertView;
    }

    public int getChildrenCount(int groupPosition)  {
        return collectionList.get(formulas.get(groupPosition)).size();
    }

    public Object getGroup(int groupPosition)   {
        return formulas.get(groupPosition);
    }

    public int getGroupCount()  {
        return formulas.size();
    }

    public long getGroupId(int groupPosition)   {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String formulaListName = (String) getGroup(groupPosition);
        if (convertView == null)    {
            LayoutInflater inflaterInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflaterInflater.inflate(R.layout.group_row, null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.group_item);
        item.setText(formulaListName);

        return convertView;
    }

    public boolean hasStableIds()   {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition)  {
        return true;
    }

}
