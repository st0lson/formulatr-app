package com.first.form;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.NumberPicker;


/**
 * Created by Steve on 7/5/13.
 */
public class Settings extends Activity {

    public static int decSpaces = 2;
    private static NumberPicker numberPicker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        numberPicker = (NumberPicker) findViewById(R.id.numberPicker);
        numberPicker.setMaxValue(10);
        numberPicker.setMinValue(0);
        numberPicker.setValue(2);
    }

    public void onPause()   {
        super.onPause();
        try{
            decSpaces = numberPicker.getValue();
        }
        catch(Exception e){
            decSpaces = 2;
        }
    }

    public void onResume()  {
        super.onResume();
        try{
            numberPicker.setValue(decSpaces);
        }
        catch(Exception e){
            numberPicker.setValue(2);
        }
    }

    public static int receiveDecSpaces() {
        try{
            decSpaces = numberPicker.getValue();
        }
        catch(Exception e){
            decSpaces = 2;
        }
        return decSpaces;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}