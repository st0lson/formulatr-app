package com.first.form;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.code.ExpandableListViewAdapter;
import com.code.equationLists;
import com.code.myFormula;

/**
 * Created by Steve on 7/5/13.
 */
public class All extends Activity {

    private ExpandableListView expLV;
    private equationLists eqList;
    private List<String> groupList;
    private List<String> childList;
    private Map<String, List<String>> formulaCollection;

    private ArrayList<myFormula> areaFormulaList;
    private ArrayList<myFormula> surfaceAreaFormulaList;
    private ArrayList<myFormula> volumeFormulaList;
    private ArrayList<myFormula> otherFormulaList;

    private ArrayList<String> areaFormulaNames;
    private ArrayList<String> surfaceAreaFormulaNames;
    private ArrayList<String> volumeFormulaNames;
    private ArrayList<String> otherFormulaNames;

    //On create
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        //Use activity all layout for color
        setContentView(R.layout.activity_all);

        //Set ExpListView to layout in XML
        expLV = (ExpandableListView) findViewById(R.id.expandableListView);

        //Initialize ExpListView Contents
        createGroupList();
        createCollection();

        //Set adapter to class adapter, send it contents
        final ExpandableListViewAdapter expListAdapter = new ExpandableListViewAdapter(this, groupList, formulaCollection);
        expLV.setAdapter(expListAdapter);

        //Set the listener to start a formula when its name is pressed
        expLV.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                //Get what child has been clicked
                final String selected = (String) expListAdapter.getChild(groupPosition, childPosition);
                Intent intent = new Intent(getApplicationContext(), Formula.class);
                Bundle b = new Bundle();

                //Send name of the formula to Formula class and start it
                b.putString("Formula", selected);
                intent.putExtras(b);
                startActivity(intent);

                //Finish onClick
                return true;
            }
        });
     }

    //Create the group list
    public void createGroupList()   {

        //get the equationLists class
        eqList = new equationLists();

        //InitializeLists
        areaFormulaList = new ArrayList<myFormula>();
        areaFormulaNames = new ArrayList<String>();
        surfaceAreaFormulaList = new ArrayList<myFormula>();
        surfaceAreaFormulaNames = new ArrayList<String>();
        volumeFormulaList = new ArrayList<myFormula>();
        volumeFormulaNames = new ArrayList<String>();
        otherFormulaList = new ArrayList<myFormula>();
        otherFormulaNames = new ArrayList<String>();

        //Put all the formulas in the eqList in the formula list
        for(myFormula f : eqList.getAreaList().values())
        {
            areaFormulaList.add(f);
            areaFormulaNames.add(f.getName());
        }

        for(myFormula f : eqList.getVolumeList().values())
        {
            volumeFormulaList.add(f);
            volumeFormulaNames.add(f.getName());
        }

        for(myFormula f : eqList.getSurfaceAreaList().values())
        {
            surfaceAreaFormulaList.add(f);
            surfaceAreaFormulaNames.add(f.getName());
        }

        for(myFormula f : eqList.getOtherList().values())
        {
            otherFormulaList.add(f);
            otherFormulaNames.add(f.getName());
        }

        //Add the two categories to the ExpandableListView
        groupList = new ArrayList<String>();
        groupList.add("Area");
        groupList.add("Volume");
        groupList.add("Surface Area");
        groupList.add("Other");
    }

    //Create the little lists
    private void createCollection() {

        //Create the child list
        formulaCollection = new LinkedHashMap<String, List<String>>();

        //For each group
        for(String group : groupList)
        {
            if (group=="Area") {
                formulaCollection.put(group, areaFormulaNames);
            }
            else if (group=="Volume") {
                formulaCollection.put(group, volumeFormulaNames);
            }
            else if (group=="Surface Area") {
                formulaCollection.put(group, surfaceAreaFormulaNames);
            }
            else if (group=="Other")  {
                formulaCollection.put(group, otherFormulaNames);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}