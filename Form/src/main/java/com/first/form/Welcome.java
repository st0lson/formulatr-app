package com.first.form;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * The Welcome Screen of Formulate!
 * How exciting. This app gives formulas, and computes them when available (usually)
 * Report to Steve.Olson05@gmail.com if you find any bugs!
 *
 * This is a pretty basic screen. Three buttons: Formulas, Favorites, and Settings.
 *
 * Created by Steve on 7/5/13.
 */

public class Welcome extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }

    //Starts Settings activity
    public void startSettings(View v) {
        Intent intent = new Intent(Welcome.this, Settings.class);
        startActivity(intent);
    }

    //Starts Favorites activity
    public void startFavorites(View v) {
        Intent intent = new Intent(Welcome.this, Favorites.class);
        startActivity(intent);
    }

    //Starts All activity (the app list)
    public void startAll(View v) {
        Intent intent = new Intent(Welcome.this, All.class);
        startActivity(intent);
    }

}