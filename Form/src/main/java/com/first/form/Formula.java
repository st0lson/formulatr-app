package com.first.form;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.code.equationLists;
import com.code.exp.Expression;
import com.code.myFormula;

/**
 * Created by Steve on 7/5/13.
 */
public class Formula extends Activity {

    //Object variables
    private int decSpaces; //Saves the amount of spaces for the displayed result
    private List<EditText> editTextList = new ArrayList<EditText>(); //The input EditText list
    private myFormula formula; //The formula object we're using
    private equationLists eqList; //The equation list that a specific formula is pulled from

    //Layout variables
    private LinearLayout linearLayout; //The linear layout
    private TextView formulaTextView; //The formula name textView
    private ImageView formulaPic; //The formula picture imageView
    private Button calcButton; //The calculate button
    private Button resetButton; //Clear Button

    //onCreate
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formula);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        //Create the formula list, get the formula name from All Activity
        eqList = new equationLists();
        Bundle b = getIntent().getExtras();
        String value = b.getString("Formula");

        //Set the formula on this page to the formula selected
        formula = eqList.getEquation(value);
        getDecSpaces();

        //Obtain handles on layout objects
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout);

        //Set Title of Formula
        formulaTextView = (TextView) findViewById(R.id.formula_title);
        formulaTextView.setText(formula.getName());
        if (formula.getName().length()>15)  {
            formulaTextView.setTextSize(formulaTextView.getTextSize()/2);
        }
        formulaTextView.setGravity(Gravity.CENTER_VERTICAL);

        //Set picture of formula
        formulaPic = (ImageView) findViewById(R.id.formula_pic);
        String imageName = formula.getName().toLowerCase().replace(" ","_");
        int id = getResources().getIdentifier(imageName, "drawable", getPackageName());
        Log.v("Get package name", getPackageName());
        formulaPic.setImageResource(id);

        //Create inputs
        for (int i = 0; i<formula.getNumVars(); i++)
        {
            EditText editText = new EditText(this);
            editText.setText(formula.getVariable(i));
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_NUMBER);
            editText.setSelectAllOnFocus(true);
            editTextList.add(editText);
            linearLayout.addView(editText);
        }

        //Create the calculate button
        calcButton = new Button(this);
        calcButton.setText("Calculate");
        calcButton.setGravity(Gravity.CENTER);
        calcButton.setWidth(R.dimen.button_width);
        calcButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                calculate();
            }
        });
        linearLayout.addView(calcButton);

        resetButton = new Button(this);
        resetButton.setText("Reset Variables");
        resetButton.setGravity(Gravity.CENTER);
        resetButton.setWidth(R.dimen.button_width);
        resetButton.setOnClickListener(new View.OnClickListener()   {
            public void onClick(View v) {
                resetVariables();
            }
        });
        linearLayout.addView(resetButton);

    }

    //Sets the desired amount of decimal spaces
    protected void getDecSpaces(){
        this.decSpaces= Settings.receiveDecSpaces();
    }

    //Resets Variables
    protected boolean isNumeric(String s)   {
        try{
            Double.parseDouble(s);
            return true;
        }
        catch( Exception e ){
                return false;
            }
    }

    //Resets Variables
    protected void resetVariables() {
        for (int i = 0; i<formula.getNumVars(); i++)
        {
            editTextList.get(i).setText(formula.getVariable(i));
        }
    }

    //Calculates the result (thanks to Expression)
    protected void calculate()  {

        //Sets the initial equation (just to avoid null error)
        String equation = formula.getFormula(0);
        int emptyCount = 0;

        //For each of the inputs, get the text from it and replace the string of the equation with the variable
        for (int i = 0; i<formula.getNumVars(); i++)
        {
            String input = editTextList.get(i).getText().toString();

            if(input.equalsIgnoreCase(formula.getVariable(i))||input.isEmpty()||input.equalsIgnoreCase("")||(!isNumeric(input)))   {
                equation = formula.getFormula(i); //Get the formula we're using (same index as variable in need)
                emptyCount++;
            }
        }

        //Replace the other inputs in the equation where the string isn't
        for (int i = 0; i<formula.getNumVars(); i++)
        {
            String input = editTextList.get(i).getText().toString();

            if(!((input.equalsIgnoreCase(formula.getVariable(i)))||input.isEmpty()||input.equalsIgnoreCase("")||(!isNumeric(input))||emptyCount>2))
            {
                equation = equation.replace(formula.getVariable(i), input);
            }
        }

        String ans = "";

        Log.v("formula before anything", equation);

        if (emptyCount==1)      {
            Expression e = new Expression(equation);
            Double answer = e.resolve();
            int dec = answer.toString().indexOf("."); //Find the decimal space
            ans = answer.toString(); //Get the result
            getDecSpaces();

            //Set the result to: (Substring in front of decimal)+(Substring (to decSpaces amount) behind decimal)
            String result = String.valueOf(BigDecimal.valueOf(Double.valueOf(ans)).setScale(decSpaces, BigDecimal.ROUND_HALF_UP).doubleValue());
            for (int i=0; i<decSpaces; i++) //For as many decimal spaces needed, add zeroes (just in case it's too small)
            {
                result+="0";
            }

            result = result.substring(0, dec)+result.substring(dec, dec+1+decSpaces);

            if (decSpaces==0)   {
                result = result.replace(".", "");
            }

            //Sets the editText field in question
            for (int i = 0; i<formula.getNumVars(); i++)
            {
                Log.v("String output", editTextList.get(i).getText().toString());

                String output = editTextList.get(i).getText().toString();

                if(output.equalsIgnoreCase(formula.getVariable(i))||output.isEmpty()||output.equalsIgnoreCase("")||(!isNumeric(output)))   {
                    editTextList.get(i).setText(result); //Display the result
                }
            }
        }

        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Wrong input!");
            builder.setMessage("Please enter all known variables.");
            builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //Do nothing! Fun.
                }
            });
            builder.show();
            for (int i = 0; i<formula.getNumVars(); i++)
            {
                editTextList.get(i).setText(formula.getVariable(i));
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
         }
        return super.onOptionsItemSelected(item);
    }

}
