package com.first.form;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.code.equationLists;
import com.code.myFormula;
import java.util.ArrayList;


/**
 * Created by Steve on 7/5/13.
 */
public class Favorites extends Activity {

    private ListView lv;
    private equationLists favList;
    private ArrayList<myFormula> formulaList;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> formulaNames;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        favList = new equationLists();

        refreshList();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String name = ((TextView) view).getText().toString();
                Intent intent = new Intent(getApplicationContext(), Formula.class);
                Bundle b = new Bundle();
                b.putString("Formula", name);
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume()   {
        super.onResume();
        refreshList();
    }

    private void refreshList()  {

        formulaList = new ArrayList<myFormula>();
        formulaNames = new ArrayList<String>();

        for(myFormula f : favList.getFavorites().values())
        {
            Log.v("In favorites", f.getName());
            formulaList.add(f);
            formulaNames.add(f.getName());
        }

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, formulaNames);

        lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}